## Instalación
```bash
#Clonar el repositorio
git clone git@gitlab.com:edgar.castro/simplechatsocketio.git

#Ingresar al directorio del proyecto
cd simplechatsocketio

# Instalar las dependencias
npm install

# Ejecutar el proyecto
npm run dev
```
